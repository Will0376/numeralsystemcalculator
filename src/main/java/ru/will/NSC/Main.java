package ru.will.NSC;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ru.will.NSC.controllers.MainController;

public class Main extends Application {
	public static Scene scene;
	public static Stage ps;
	public static Main instance;
	public static boolean debug = false;
	public static boolean dark = true;
	public static String ver = "1.0";

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		instance = this;
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ru/will/NSC/Main.fxml"));
		Parent root = loader.load();

		primaryStage.setTitle("NumeralSystemCalculator");
		primaryStage.setScene(new Scene(root, 600, 400));

		primaryStage.setResizable(false);
		primaryStage.show();

		scene = primaryStage.getScene();
		ps = primaryStage;
		if (dark) {
			scene.getStylesheets().add(getClass().getClassLoader().getResource("ru/will/NSC/dark-theme.css").toString());
			MainController.instance.MIdark.setText(MainController.instance.MIdark.getText() + " ✓");
			MainController.instance.text.setFill(Color.WHITE);
		}
	}
}
