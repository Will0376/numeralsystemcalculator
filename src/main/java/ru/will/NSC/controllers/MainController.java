package ru.will.NSC.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import ru.will.NSC.Main;

import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MainController implements Initializable {
	public static MainController instance;
	@FXML
	public MenuItem MIdark;
	@FXML
	public Text text;
	@FXML
	ComboBox<Integer> CBfrom;
	@FXML
	ComboBox<Integer> CBto;
	@FXML
	TextField TFinput;
	@FXML
	TextArea TAoutput;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		instance = this;
		printToLog("Init! NSC version: " + Main.ver + " build: " + getBuildTime());
		ObservableList<Integer> observableList = FXCollections.observableArrayList(Arrays.stream(IntStream.range(2, 33).toArray()).boxed().collect(Collectors.toList()));
		CBfrom.setItems(observableList);
		CBto.setItems(observableList);
		TAoutput.setEditable(false);
	}

	public void convert() {
		String input = TFinput.getText();
		if (input.isEmpty() || CBfrom.getValue() == null || CBto.getValue() == null) {
			printToLog("error");
			return;
		}
		if (!checkValid(TFinput.getText())) {
			printToLog("invalid text");
			return;
		}
		String out;
		if (CBfrom.getValue() != 10 && CBto.getValue() != 10) {
			out = toNeed(toTen(TFinput.getText()));
		} else if (CBfrom.getValue() == 10) {
			out = toNeed(TFinput.getText());
		} else {
			out = toTen(TFinput.getText());
		}
		printToLog("Result: " + out);
	}

	private String toNeed(String in) {
		String ret;
		if (in.contains(",") || in.contains(".")) {
			String[] split = in.split("[,.]");
			List<String> beforeSplit = toComma(split[0]);
			Collections.reverse(beforeSplit);

			List<String> afterSplit = new ArrayList<>();
			String beforeStr, afterStr;
			int count = 0;
			double db = Double.parseDouble("0." + split[1]);

			while (true) {
				double tmp = db * CBto.getValue();
				String e = numToLet(Integer.parseInt(String.valueOf(tmp).split("\\.")[0]));
				if (e == null) {
					return "null";
				}
				afterSplit.add(e);
				db = Double.parseDouble("0." + String.valueOf(tmp).split("\\.")[1]);
				if (count == 10) break;
				count++;
			}
			beforeStr = String.join("", beforeSplit);
			afterStr = String.join("", afterSplit);
			ret = beforeStr + "." + afterStr;
		} else {
			List<String> beforeSplit = toComma(in);
			Collections.reverse(beforeSplit);
			ret = String.join("", beforeSplit);
		}
		return ret;
	}

	private List<String> toComma(String in) {
		List<String> beforeSplit = new ArrayList<>();
		int prev = Integer.parseInt(in);
		do {
			String e = numToLet(prev % CBto.getValue());
			if (e == null) {
				printToLog("NULL");
				return beforeSplit;
			}
			beforeSplit.add(e);
			prev = prev / CBto.getValue();
		} while (prev > 0);

		return beforeSplit;
	}

	private String numToLet(int in) {
		char[] let = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		if (in <= 9) return String.valueOf(in);
		else for (int i = 0; i < 25; i++) {
			if ((in - 10) == i) return Character.toString(let[i]).toUpperCase();
		}
		return null;
	}

	private String toTen(String in) {
		String ret;
		if (in.contains(",") || in.contains(".")) {
			String[] split = in.split("[,.]");
			String[] bef = split[0].split("");
			String[] aft = split[1].split("");
			double beforesplit = 0, aftersplit = 0;
			int afterpoint = 0;
			int beforelength = bef.length;

			for (String s : bef) {
				--beforelength;
				beforesplit = beforesplit + mult(s, beforelength);
			}
			for (String s : aft) {
				--afterpoint;
				aftersplit = aftersplit + mult(s, afterpoint);
			}
			ret = String.valueOf(beforesplit + aftersplit);
		} else {
			String[] split = in.split("");
			int length = split.length;
			double splitting = 0D;
			for (String s : split) {
				--length;
				splitting = splitting + mult(s, length);
			}
			ret = String.valueOf(splitting);
			if (ret.contains(".0")) ret = ret.replace(".0", "");
		}
		return ret;
	}

	private Double mult(String s, int length) {
		double i = letToNum(s);
		if (length >= 0) return i * (int) Math.pow(CBfrom.getValue(), length);
		else return i * Math.pow(CBfrom.getValue(), length);
	}

	private boolean checkValid(String in) {
		char[] text = in.replaceAll("[,.]", "").toCharArray();
		for (char ch : text) {
			if (letToNum(String.valueOf(ch)) >= CBfrom.getValue()) {
				return false;
			}
		}
		return true;
	}

	private double letToNum(String in) {
		String let = "abcdefghijklmnopqrstuvwxyz";
		if (in.matches("\\d")) {
			return Double.parseDouble(in);
		}
		for (String tmp : let.split("")) {
			if (in.toLowerCase().equals(tmp)) {
				return 10 + let.indexOf(tmp);
			}
		}
		return 0;
	}

	public void setDark() {
		if (Main.dark) {
			MIdark.setText(MIdark.getText().replace(" ✓", ""));
			Main.ps.getScene().getStylesheets().remove(Objects.requireNonNull(getClass().getClassLoader().getResource("ru/will/NSC/dark-theme.css")).toString());
			text.setFill(Color.BLACK);
		} else {
			MIdark.setText(MIdark.getText() + " ✓");
			Main.ps.getScene().getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("ru/will/NSC/dark-theme.css")).toString());
			text.setFill(Color.WHITE);
		}
		Main.dark = !Main.dark;
	}

	public void exit() {
		Platform.exit();
	}

	public void printToLog(String text) {
		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			TAoutput.appendText("\n" + sdf.format(cal.getTime()) + "->: " + text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getBuildTime() {
		URLClassLoader cl = (URLClassLoader) getClass().getClassLoader();
		try {
			URL url = cl.findResource("META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest(url.openStream());
			Attributes mainAttributes = manifest.getMainAttributes();
			String temp = mainAttributes.getValue("Build-Timestamp");
			if (temp.isEmpty()) throw new NullPointerException();
			return temp;
		} catch (Exception E) {
			return "null or dev";
		}
	}

}
